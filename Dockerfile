FROM nginx:alpine
COPY react-nginx-server.conf /etc/nginx/nginx.conf
COPY build/ /usr/share/nginx/html/
