import React, { Component } from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { refreshAuthBearer } from './redux/actions';

import './App.css'

import Routes from './routes'

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: { main: '#223C6F' },
    secondary: { main: '#3F95EA' },
  },
});

class App extends Component {    
  
  async componentDidMount() {
    await this.props.refreshAuthBearer()
  }
  
  isAuth() {
    return this.props.userId ? '' : ''
  }
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <div className="App">
          <Routes></Routes>
        </div>
        {this.isAuth()}
      </MuiThemeProvider>
    );
  }
}
const mapStateToProps = (state) => {
  const { userId } = state.auth;
  return { userId };
};
const mapDispatchToProps = { refreshAuthBearer };

export default connect(mapStateToProps, mapDispatchToProps)(App);
