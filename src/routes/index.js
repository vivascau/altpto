import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUserAccount, getCurrentUser, refreshAuthBearer } from '../redux/actions';

import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import Login from 'pages/Login/Login';
import MFA from 'pages/MFA/MFA';
import Dashboard from 'pages/Dashboard/Dashboard';
import Transactions from 'pages/Transactions/Transactions';
import TransactionDetails from 'pages/TransactionDetails/TransactionDetails';
import Addresses from 'pages/Addresses/Addresses';
import AddressesNew from 'pages/AddressesNew/AddressesNew';
import Payment from 'pages/Payment/Payment';
import Topup from 'pages/Topup/Topup';
import Withdraw from '../pages/Withdraw/Withdraw';
import NotFound from '../pages/404/404';
import AppHeader from '../containers/AppHeader/AppHeader';
import AppFooter from '../containers/AppFooter/AppFooter';
import Settings from '../pages/Settings/Settings';
import Payees from '../pages/Payees/Payees';
import AddressDetails from '../pages/AddressDetails/AddressDetails';

class Routes extends Component {
  async componentWillMount() {
    await this.props.refreshAuthBearer()
    await this.props.getCurrentUser();
    await this.props.getUserAccount();
    console.debug('APP LOADED!')
  }

  render() {
    return (
      <Router>
        <div>
          <AppHeader></AppHeader>
          <Switch>
            <Route exact={ true } path="/">
              <Redirect to="/login"></Redirect>
            </Route>
            <Route path="/login" component={ Login }></Route>
            <Route path="/mfa" component={ MFA }></Route>
            <Route path="/dashboard" component={ Dashboard }></Route>
            <Route path="/settings" component={ Settings }></Route>
            <Route path="/topup" isExact="true" component={ Topup }></Route>
            <Route path="/withdraw" isExact="true" component={ Withdraw }></Route>
            <Route path="/transactions/:id" component={ TransactionDetails }></Route>
            <Route path="/transactions" isExact="true" component={ Transactions }></Route>
            <Route path="/payees/:id/payment" component={ Payment }></Route>
            <Route path="/addresses/new" isExact="true" component={ AddressesNew }></Route>
            <Route path="/addresses/:id" isExact="true" component={ AddressDetails }></Route>
            <Route path="/payees" isExact="true" component={ Payees }></Route>
            <Route path="/addresses" isExact="true" component={ Addresses }></Route>
            <Route path="*" component={ NotFound }></Route>
          </Switch>
          <AppFooter></AppFooter>
        </div>
      </Router>
    );
  }
}

const mapStateToProps = state => {
  const { available, balance } = state.account;
  return { available, balance };
};
const mapDispatchToProps = { getUserAccount, getCurrentUser, refreshAuthBearer };

export default connect(mapStateToProps, mapDispatchToProps)(Routes);