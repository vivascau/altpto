import React, { Component } from 'react';
import './TopupItem.css'

export default class TopupItem extends Component {
  render() {
    return (
      <div className="TopupItem">
        <div className="TopupItem-head">
         { this.props.title }
        </div>
        <div className="TopupItem-val">
          { this.props.children }
        </div>
      </div>
    )
  }
}
