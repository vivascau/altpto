import React, { Component } from 'react';
import './TransactionHistoryTime.css';

export default class TransactionHistoryTime extends Component {
  static defaultProps = {
    theme: '',
    time: ''
  }

  render() {
    const { theme, time } = this.props;

    return (
      <div className={`${theme} TransactionHistoryTime`}>
          { time }
      </div>
    )
  }
}
