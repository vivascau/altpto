import React, { Component } from 'react';
import './PageContent.css'
import { Grid } from '@material-ui/core';

export default class PageContent extends Component {
  render() {
    return (
      <div className="PageContent-container">
        <Grid
          container
          direction="column"
          justify="flex-start"
          alignItems="center">
            { this.props.children }
        </Grid>
      </div>
    )
  }
}
