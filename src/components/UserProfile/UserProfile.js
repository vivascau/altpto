import React, { Component } from 'react';

import { Grid } from '@material-ui/core';
import './UserProfile.css';

import Divider from '../Divider/Divider';

export default class UserProfile extends Component {
  renderFirstName() {
    if (this.props.firstName) {
      return(
      <span>
        as { this.props.firstName }
      </span>)
    }
  }

  render() {
    const { onLogout } = this.props;

    return (
      <div className="UserProfile">
        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="stretch"
        >
        <div>Authenticated { this.renderFirstName() } </div>
        <div className="UserProfile-logout pointer" onClick={() => onLogout()}>
          <div className="UserProfile-logoutText">Logout</div>
        </div>
      </Grid>
      <Divider size="8"></Divider>
    </div>
    )
  }
}
