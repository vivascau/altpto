import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import './ActionButton.css';


class ActionButton extends Component {
  navigateToPath = () => {
    if (this.props.path) {
      this.props.history.push(this.props.path)
    }
  }

  render() {
    return (
      <div className="ActionButton pointer" onClick={this.navigateToPath}>
        { this.props.children }
      </div>
    )
  }
}

export default withRouter(ActionButton)