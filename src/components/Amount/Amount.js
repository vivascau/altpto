import React, { Component } from 'react';
import './Amount.css';

export default class Amount extends Component {
  static defaultProps = {
    theme: 'small',
    amount: '0'
  }

  render() {
    const { theme, amount } = this.props;

    return (
      <div className={`${theme} Amount`}>
          € { amount }
      </div>
    )
  }
}
