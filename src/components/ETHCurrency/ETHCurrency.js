import React, { Component } from 'react';

import ETHIcon from '../ETHIcon/ETHIcon';
import PaymentCurrency from '../PaymentCurrency/PaymentCurrency';

export default class ETHCurrency extends Component {
  render() {
    return (
      <PaymentCurrency label="ETH">
        <ETHIcon></ETHIcon>
      </PaymentCurrency>
    )
  }
}
