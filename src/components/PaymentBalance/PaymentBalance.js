import React, { Component } from 'react';
import { Grid } from '@material-ui/core';

import './PaymentBalance.css';

class PaymentBalance extends Component {
  render() {
    return (
        <Grid
            onClick={this.props.onClick}
            className="PaymentBalance pointer"
            container
            direction="row"
            justify="center"
            alignItems="center"
          >
            <div className="PaymentBalance-title">{this.props.title}</div>
            { this.props.children }
          </Grid>
    )
  }
}

export default PaymentBalance

