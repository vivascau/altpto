import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withRouter } from 'react-router-dom';
import './AddressSearchBar.css';
import { TextField } from '@material-ui/core';

class AddressSearchBar extends Component {
  render() {
    const { placeholder } = this.props;
    return (
      <div className="AddressSearchBar">
        <TextField
          onChange={(ev) => this.props.onChange(ev)}
          id="outlined-search"
          label="Search"
          placeholder={placeholder}
          fullWidth
          type="search"
          className="AddressSearchBar-field"
          margin="normal"
          variant="outlined"
        />
      </div>
    )
  }
}
AddressSearchBar.propTypes = {
  onChange: PropTypes.func.isRequired,
};

export default withRouter(AddressSearchBar)