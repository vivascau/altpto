import React, { Component } from 'react';

import './AddressIcon.css';

export default class AddressIcon extends Component {
  renderAddressIcon () {
    return(
      <svg width="42" height="43" viewBox="0 0 42 43" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g clipPath="url(#clip0)">
          <path d="M10.9534 12.5151V28.8484C10.9534 30.1318 11.9917 31.1818 13.2867 31.1818H29.62C30.9034 31.1818 31.9534 30.1318 31.9534 28.8484V12.5151C31.9534 11.2318 30.9034 10.1818 29.62 10.1818H13.2867C11.9917 10.1818 10.9534 11.2318 10.9534 12.5151ZM24.9534 17.1818C24.9534 19.1184 23.39 20.6818 21.4534 20.6818C19.5167 20.6818 17.9534 19.1184 17.9534 17.1818C17.9534 15.2451 19.5167 13.6818 21.4534 13.6818C23.39 13.6818 24.9534 15.2451 24.9534 17.1818ZM14.4534 26.5151C14.4534 24.1818 19.12 22.8984 21.4534 22.8984C23.7867 22.8984 28.4534 24.1818 28.4534 26.5151V27.6818H14.4534V26.5151Z" fill="white"/>
          <circle cx="21" cy="21.1818" r="20.5" stroke="#3F95EA"/>
        </g>
        <defs>
          <clipPath id="clip0">
            <rect width="60.8696" height="60.8696" fill="white" transform="translate(0 0.181763) scale(0.69)"/>
          </clipPath>
        </defs>
      </svg>)
  }
  
  renderNegativeAddressIcon () {
    return(
      <svg width="45" height="42" viewBox="0 0 45 42" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g clipPath="url(#clip1)">
          <path d="M11.2567 12.3333V28.6667C11.2567 29.95 12.295 31 13.59 31H29.9234C31.2067 31 32.2567 29.95 32.2567 28.6667V12.3333C32.2567 11.05 31.2067 10 29.9234 10H13.59C12.295 10 11.2567 11.05 11.2567 12.3333ZM25.2567 17C25.2567 18.9367 23.6934 20.5 21.7567 20.5C19.82 20.5 18.2567 18.9367 18.2567 17C18.2567 15.0633 19.82 13.5 21.7567 13.5C23.6934 13.5 25.2567 15.0633 25.2567 17ZM14.7567 26.3333C14.7567 24 19.4234 22.7167 21.7567 22.7167C24.09 22.7167 28.7567 24 28.7567 26.3333V27.5H14.7567V26.3333Z" fill="#223C6F"/>
          <circle cx="21.3033" cy="21" r="20.5" stroke="#3F95EA"/>
        </g>
        <defs>
          <clipPath id="clip1">
            <rect width="63.5073" height="60.8696" fill="#3F95EA" transform="translate(0.303345) scale(0.69)"/>
          </clipPath>
        </defs>
      </svg>)
  }
  
  render() {
    return (
      <div className="AddressIcon">
        { this.props.type === "negative" ? this.renderNegativeAddressIcon() : this.renderAddressIcon() }
      </div>
    )
  }
}
