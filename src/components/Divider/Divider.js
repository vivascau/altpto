import React, { Component } from 'react';
import './Divider.css';

export default class Divider extends Component {
  static defaultProps = {
    size: 1,
    unit: "px"
  }

  render() {
    return (
      <div style={{height: `${this.props.size}${this.props.unit}`}}></div>
    )
  }
}
