import React, { Component } from 'react';

import BCHIcon from '../BCHIcon/BCHIcon';
import PaymentCurrency from '../PaymentCurrency/PaymentCurrency';

export default class BCHCurrency extends Component {
  render() {
    return (
      <PaymentCurrency label="BCH">
        <BCHIcon></BCHIcon>
      </PaymentCurrency>
    )
  }
}
