import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import Time from 'react-time-format';

import './TransactionHistoryUpdate.css';

export default class TransactionHistoryUpdate extends Component {
  render() {
    const { update, time } = this.props;
    return (
      <div className="TransactionHistoryUpdate">
        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="center"
        >
          <div className="TransactionHistoryUpdate-time">
            <Time value={new Date(time)} format="DD/MM/YY HH:mm"/>
          </div>
          <div className="TransactionHistoryUpdate-label">{ update }</div>
        </Grid>
      </div>
    )
  }
}
