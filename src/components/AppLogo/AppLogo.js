import React, { Component } from 'react';
import './AppLogo.css'

export default class AppLogo extends Component {
  render() {
    return (
      <div className="AppLogo">
          <img alt="Logo" src="/altalixLogo.png"></img>
      </div>
    )
  }
}
