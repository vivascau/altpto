import React, { Component } from 'react';
import './Info.css'

export default class Info extends Component {
  renderTitle() {
    const { title } = this.props;
    if (title) {
      return(
        <div className="Info-title">
          { title }
        </div>
      )
    }
  }

  render() {
    return (
      <div className={`Info-container ${this.props.withBorder ? 'withBorder' : ''}`}>
        { this.renderTitle() }
        <div className="Info-content">
          { this.props.children }
        </div>
      </div>
    )
  }
}
