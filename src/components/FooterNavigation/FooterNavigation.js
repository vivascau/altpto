import React , { Component }from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import EuroSymbolIcon from '@material-ui/icons/EuroSymbol';
import RestoreIcon from '@material-ui/icons/Restore';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import SettingsIcon from '@material-ui/icons/Settings';

const styles = {
  root: {
    width: '100vw'
  },
};

class FooterNavigation extends Component {
  state = {
    value: '',
    path: {
      0: '/dashboard',
      1: '/transactions',
      2: '/addresses',
      3: '/settings'
    }
  };

  handleChange = (event, value) => {
    this.setState({ value });
    this.props.onChange(this.state.path[value])
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    return (
      <BottomNavigation
        value={value}
        onChange={this.handleChange}
        showLabels
        className={classes.root}
      >
        <BottomNavigationAction label="Balance" icon={<EuroSymbolIcon/>} />
        <BottomNavigationAction label="Transactions" icon={<RestoreIcon />} />
        <BottomNavigationAction label="Addresses" icon={<PermIdentityIcon />} />
        <BottomNavigationAction label="Settings" icon={<SettingsIcon />} />
      </BottomNavigation>
    );
  }
}

FooterNavigation.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FooterNavigation);