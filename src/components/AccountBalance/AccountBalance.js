import React, { Component } from 'react';
import  './AccountBalance.css';
import Amount from '../Amount/Amount';
import AccountCard from '../AccountCard/AccountCard';

export default class AccountBalance extends Component {
  render() {
    return (
      <AccountCard title="Account Balance" flatBottom={this.props.flatBottom}>
        <Amount theme="big" amount={this.props.amount}></Amount>
      </AccountCard>
    )
  }
}
