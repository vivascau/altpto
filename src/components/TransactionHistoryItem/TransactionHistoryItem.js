import React, { Component } from 'react';
import { Grid } from '@material-ui/core';

import './TransactionHistoryItem.css';
import TransactionHistoryType from '../TransactionHistoryType/TransactionHistoryType';

export default class TransactionHistoryItem extends Component {
  static defaultProps = {
    walletName: 'wallet',
    status: 'status',
    type: 'type',
    amount: 1,
    currency: 'ETH'
  }
  
  onClick(tradeId) {
    console.log('LOAD TRADE: ', tradeId)
  }

  render() {
    const { onClick, tradeId, walletName, status, type, amount, currency, sellVolume } = this.props;

    return (
      <Grid
        onClick={() => onClick(tradeId)}
        className="TransactionHistoryItem pointer"
        container
        direction="row"
        justify="flex-start"
        alignItems="stretch"
      >
        <TransactionHistoryType className="TransactionHistoryItem-type" type={type}></TransactionHistoryType>
        <div className="TransactionHistoryItem-wallet">
          <div className="TransactionHistoryItem-head">{ walletName }</div>
          <div className={status}>{ status }</div>
        </div>
        <div className="TransactionHistoryItem-amount">
          <div className="TransactionHistoryItem-head">- { currency } { amount }</div>
          <div>€{ sellVolume }</div>
        </div>
      </Grid>
    )
  }
}
