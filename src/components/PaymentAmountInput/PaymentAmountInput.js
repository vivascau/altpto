import React, { Component } from 'react';
import { Grid } from '@material-ui/core';

import './PaymentAmountInput.css';

class PaymentAmountInput extends Component {
  render() {
    return (
        <Grid
            className="PaymentAmountInput"
            container
            direction="row"
            justify="center"
            alignItems="center"
          >
            <div className="PaymentAmountInput-currency halfWidth">
              { this.props.children[0] }
            </div>
            <div className="PaymentAmountInput-value halfWidth">
              { this.props.children[1] }
            </div>            
          </Grid>
    )
  }
}

export default PaymentAmountInput;
