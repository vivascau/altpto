import React, { Component } from 'react';

import './ETHIcon.css';

export default class ETHIcon extends Component {
  render() {
    return (
      <div className="ETHIcon">
        <svg width="16" height="24" viewBox="0 0 16 24" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect width="15.2727" height="24" fill="none"/>
          <path opacity="0.600006" d="M7.63423 8.98627L0.432251 12.2147L7.63423 16.4131L14.8362 12.2147L7.63423 8.98627Z" fill="#010101"/>
          <path opacity="0.449997" d="M0.432251 12.2146L7.63423 16.4131V8.98621V0.42926L0.432251 12.2146Z" fill="#010101"/>
          <path opacity="0.800003" d="M7.63428 0.42926V8.98621V16.4131L14.8362 12.2146L7.63428 0.42926Z" fill="#010101"/>
          <path opacity="0.449997" d="M0.432251 13.5615L7.63423 23.5707V17.7579L0.432251 13.5615Z" fill="#010101"/>
          <path opacity="0.800003" d="M7.63428 17.7579V23.5707L14.8405 13.5616L7.63428 17.7579Z" fill="#010101"/>
        </svg>
      </div>
    )
  }
}
