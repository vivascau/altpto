import React, { Component } from 'react';

import EURIcon from '../EURIcon/EURIcon';
import PaymentCurrency from '../PaymentCurrency/PaymentCurrency';

export default class EURCurrency extends Component {
  render() {
    return (
      <PaymentCurrency label="EUR">
        <EURIcon></EURIcon>
      </PaymentCurrency>
    )
  }
}
