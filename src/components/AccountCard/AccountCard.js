import React, { Component } from 'react';
import  './AccountCard.css';

export default class AccountCard extends Component {
  render() {
    return (
      <div className={`AccountCard ${this.props.flatBottom ? 'flatBottom' : ''}`}>
        <div className="AccountCard-title">{ this.props.title }</div>
        { this.props.children }
      </div>
    )
  }
}
