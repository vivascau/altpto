import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import './QuickAction.css';


class QuickAction extends Component {
  navigateToPath = () => {
    this.props.history.push(this.props.path)
  }

  render() {
    return (
      <div className="QuickAction pointer" onClick={this.navigateToPath}>
        { this.props.children }
      </div>
    )
  }
}

export default withRouter(QuickAction)