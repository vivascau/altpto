import React, { Component } from 'react';
import { Grid } from '@material-ui/core';

import './TransactionHistoryStatus.css';

export default class TransactionHistoryStatus extends Component {
  render() {
    const { status } = this.props;
    return (
      <div className={`TransactionHistoryStatus ${status}`}>
        <Grid
          container
          direction="column"
          justify="space-between"
          alignItems="center"
        >
          <div className="TransactionHistoryStatus-label">{ status }</div>
        </Grid>
      </div>
    )
  }
}
