import React, { Component } from 'react';
import { Grid } from '@material-ui/core';

import './AddressItem.css';
import AddressIcon from 'components/AddressIcon/AddressIcon';

export default class AddressItem extends Component {
  static defaultProps = {
    walletName: '',
    contactName: '',
    address: '',
    currency: ''
  }

  renderCurrency(currency) {
    if (currency) {
      return (<span className="AddressItem-currency">{ currency }</span>)
    }
  }

  render() {
    const { walletName, address, currency, onClick, id } = this.props;
  
    return (
      <Grid
        onClick={() => onClick(id)}
        className={`AddressItem pointer ${this.props.type ? this.props.type : ''}`}
        container
        direction="row"
        justify="flex-start"
        alignItems="stretch"
      >
        <AddressIcon className="AddressItem-icon" type={this.props.type}></AddressIcon>
        <div className="AddressItem-wallet">
          <div className="AddressItem-head">{ walletName }</div>
          <div className="AddressItem-details">
            { this.renderCurrency(currency) }<div className="AddressItem-address">{ address }</div>
          </div>
        </div>
      </Grid>
    )
  }
}
