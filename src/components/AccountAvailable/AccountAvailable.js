import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import Amount from '../Amount/Amount';

import './AccountAvailable.css';

export default class AccountAvailable extends Component {
  render() {
    const { availableBalance } = this.props;
    return (
      <div className="AccountAvailable">
        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="center"
        >
          <div className="AccountAvailable-label">Available</div>
          <Amount amount={availableBalance}></Amount>
        </Grid>
      </div>
    )
  }
}
