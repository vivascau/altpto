import React, { Component } from 'react';
import { Grid } from '@material-ui/core';

import './PaymentVerticalDivider.css';

class PaymentVerticalDivider extends Component {
  render() {
    return (
        <Grid
            className="PaymentVerticalDivider"
            container
            direction="row"
            justify="center"
            alignItems="center"
          >
            <div className="PaymentVerticalDivider-left"></div>
            <div className="PaymentVerticalDivider-right"></div>
          </Grid>
    )
  }
}

export default PaymentVerticalDivider

