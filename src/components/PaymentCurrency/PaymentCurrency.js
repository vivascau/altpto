import React, { Component } from 'react';

import './PaymentCurrency.css';
import { Grid } from '@material-ui/core';

export default class PaymentCurrency extends Component {
  render() {
    return (
      <Grid
        className="PaymentCurrency"
        container
        alignContent="stretch"
        direction="row"
        justify="center"
        alignItems="center"
      >
        { this.props.children }
        <div className="PaymentCurrency-label">{ this.props.label }</div>
      </Grid>
    )
  }
}
