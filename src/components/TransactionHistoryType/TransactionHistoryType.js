import React, { Component } from 'react';

import './TransactionHistoryType.css';
import OutgoingIcon from '../OutgoingIcon/OutgoingIcon';

export default class TransactionHistoryType extends Component {
  static defaultProps = {
    type: 'type'
  }

  render() {
    const { type } = this.props;
    // TODO - Create each svg type in it's own component
    return (
      <div className={`${type} TransactionHistoryType`}>
        <OutgoingIcon></OutgoingIcon>
      </div>
    )
  }
}
