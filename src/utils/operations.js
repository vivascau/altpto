const ZEROS = 100;

const getIntValue = (n) => {
  return n * ZEROS;
}

const getDecValue = (n) => {
  return n / ZEROS;
}

export {
  getIntValue,
  getDecValue
}