import axios from 'axios';
import { FIXED_SELL_CURRENCY, QUOTE_TYPE } from './quotes';

const axiosInstance = axios.create({
  baseURL: `${process.env.REACT_APP_API_URL}`,
  timeout: 10000
});

const updateBearerToken = token => {
  axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${token}`;
}

const post = (path, body) => {
  return axiosInstance.post(path, body)
}

const get = (path) => {
  return axiosInstance.get(path)
}

const loginUser = async (email, password, mfa) => {
  const res = await post('/authorization', {
    email, password, totp_code: mfa
  });
  return res.data.access_token;
}

const getCurrentUser = async () => {
  const res = await get('/users');
  return res.data && res.data[0];
}

const getUserAccount = async (userId) => {
  const res = await get(`/users/${userId}/account`);
  return res.data;
}

const createUserAccount = async (first_name, last_name, date_of_birth, email, password) => {
  const res = await post('/users', {
    first_name, last_name, date_of_birth, email, password
  });
  return res.data;
}

const getBeneficiaries = async () => {
  const res = await get('/beneficiaries');
  return res.data;
}

const getBeneficiary = async (id) => {
  const res = await get(`/beneficiaries/${id}`);
  return res.data;
}

const createBeneficiary = async (name, display_name, crypto_currency, crypto_address) => {
  const res = await post('/beneficiaries', {
    name, display_name, crypto_currency, crypto_address
  });
  return res.data;
}

const createQuote = async (beneficiary_id, 
  buy_currency, 
  sell_currency=FIXED_SELL_CURRENCY, 
  fixed_side=QUOTE_TYPE.BUY, 
  buy_volume, 
  sell_volume) => {
    const res = await post('/quotes', {
      beneficiary_id, buy_currency, sell_currency, fixed_side, buy_volume, sell_volume
    });
    return res.data;
}

const createTrade = async (
  quote_id,
  beneficiary_id,
  buy_currency,
  buy_volume,
  sell_currency,
  sell_volume,
  settlement_fee,
  price) => {
    const res = await post('/trades', {
      quote_id,
      beneficiary_id,
      buy_currency,
      buy_volume: parseFloat(buy_volume),
      sell_currency,
      sell_volume: parseFloat(sell_volume),
      settlement_fee: parseFloat(settlement_fee),
      price: parseFloat(price)
    });
    return (res.data && res.data.id);
}

const getTrades = async () => {
  const res = await get('/trades');
  return res.data;
}

const getCurrencies = async () => {
  const res = await get('/currencies');
  return res.data;
}

const getTrade = async (id) => {
  const res = await get(`/trades/${id}`);
  return res.data;
}

export default {
  get,
  post,
  loginUser,
  getCurrentUser,
  getUserAccount,
  createUserAccount,
  updateBearerToken,
  getBeneficiaries,
  getBeneficiary,
  createBeneficiary,
  createQuote,
  createTrade,
  getTrades,
  getCurrencies,
  getTrade
}