export const QUOTE_TYPE = {
  BUY: 'BUY',
  SELL: 'SELL'
} 

export const FIXED_SELL_CURRENCY = 'EUR'
export const FIXED_SETTLEMENT_FEE_CURRENCY = 'ETH'