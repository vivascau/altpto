import React, { Component } from 'react';
import './QuickActions.css';
import { Grid } from '@material-ui/core';
import QuickAction from '../../components/QuickAction/QuickAction';

export default class QuickActions extends Component {
  state = {
    title: 'Quick Actions',
    actions: [
      {
        name: 'Top Up Account',
        icon: '',
        path: '/topup'
      },
      {
        name: 'New Payment',
        icon: '',
        path: '/payees'
      },
      {
        name: 'Withdraw Money',
        icon: '',
        path: '/withdraw'
      }
    ]
  }

  quickActions = () => {
    const { actions } = this.state;
    return actions.map((action, i) =>(
        <Grid className="QuickAction-container" item key={i}>
          <QuickAction className="QuickAction" path={action.path}>{action.name}</QuickAction>
        </Grid>)
    );
  }
  
  render() {
    return (
      <div className="QuickActions">
        <div className="QuickActions-title">{this.state.title}</div>
        <Grid container justify="space-around">
          { this.quickActions() }
        </Grid>
      </div>
    )
  }
}
