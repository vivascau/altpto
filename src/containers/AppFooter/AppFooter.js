import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import './AppFooter.css'
import FooterNavigation from '../../components/FooterNavigation/FooterNavigation';

class AppFooter extends Component {
  onChange = (value) => {
    this.props.history.push(value)
  }

  render() {
    return (
      <div className={`AppFooter topShadow ${!this.props.footer.visible ? 'hidden' : ''}`}>
        <FooterNavigation onChange={this.onChange}></FooterNavigation>
      </div>
    )
  }
}
const mapStateToProps = state => {
  const { footer } = state.route;
  return { footer };
};
const mapDispatchToProps = { };
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppFooter));
