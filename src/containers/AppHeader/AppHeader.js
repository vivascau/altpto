import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import './AppHeader.css'

class AppHeader extends Component {
  goBack = () => {
    this.props.history.goBack()
  }

  renderBackButton = () => {
    if (this.props.header.withBackButton) {
      return (<div className="AppHeader-back pointer" onClick={this.goBack}><KeyboardArrowLeftIcon /></div>)
    } else {
      return (<div className="AppHeader-back pointer">&nbsp;</div>)
    }
  }

  render() {
    return (
      <div className="AppHeader bottomShadow">
        { this.renderBackButton ()}
        <div className="AppHeader-title">
          <img className="AppHeader-logo" alt="Logo" src="/altalix_text.png"></img>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { header } = state.route;
  return { header };
};
const mapDispatchToProps = { };
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppHeader));
