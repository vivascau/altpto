import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './NewContactFields.css'
import { TextField, Button, withStyles, MenuItem } from '@material-ui/core';

const styles = theme => ({
  container: {
    width: '100vw',
    textAlign: 'center'
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    width: '280px',
    textAlign: 'start'
  }
});

class NewContactFields extends Component {
    state = {
      name: '',
      address: '',
      currency: '',
      valid: {
        name: true,
        address: true
      }
    };
    composeDisplayName() {
      return `${this.state.name} ${this.state.currency} Wallet`;
    }

    onSubmit = event => {
      event.preventDefault()
      this.onConfirm()
    };

    onConfirm = () => {
      this.props.onConfirm(this.state.name, this.composeDisplayName() ,this.state.currency, this.state.address)
    };

    handleChange = (name, skipValidityCheck) => event => {
      let valid = {...this.state.valid}
      valid[name] = skipValidityCheck || event.target.checkValidity();
      this.setState({
        [name]: event.target.value,
        valid
      });
    };

    formNotValid = () => {
      return !this.state.name || !this.state.address || !this.state.currency
    }

    render() {
      const { classes, currencies } = this.props;
      return (
        <div className={classes.container}>
          <form onSubmit={this.onSubmit}>
            <div>
              <TextField
                className={classes.input}
                id="name"
                value={this.state.name}
                onChange={this.handleChange('name')}
                error={!this.state.valid.name}
                label="Payee name"
                type="text"
                required
                margin="normal"
              />
            </div>
            <div>
              <TextField
                className={classes.input}
                id="address"
                value={this.state.address}
                onChange={this.handleChange('address')}
                error={!this.state.valid.address}
                label="Address"
                type="text"
                required
              />
            </div>
            <div>
              <TextField
                id="standard-select-currency"
                select
                label="Currency"
                className={classes.input}
                value={this.state.currency}
                onChange={this.handleChange('currency', true)}
                SelectProps={{
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
                helperText="Please select currency"
                margin="normal"
              >
              {currencies.map(currency => (
                <MenuItem key={currency.currency_code} value={currency.currency_code}>
                  { currency.name } ({ currency.currency_code })
                </MenuItem>
              ))}
              </TextField>
            </div>
            <Button className={classes.button}
                    type="submit"
                    disabled={this.formNotValid()}
                    variant="contained" 
                    color="primary">
              Confirm
            </Button>
          </form>
        </div>
      );
    }
  }

NewContactFields.propTypes = {
  classes: PropTypes.object.isRequired,
  onConfirm: PropTypes.func.isRequired,
};

export default withStyles(styles)(NewContactFields);
