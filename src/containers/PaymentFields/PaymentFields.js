import React, { Component } from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash.debounce';
import { QUOTE_TYPE } from '../../utils/quotes';

import { connect } from 'react-redux';
import { 
  quoteIsValidSet,
  getBeneficiary,
  quoteBeneficiarySet,
  quoteSellVolumeSet,
  quoteBuyVolumeSet,
  quoteFixedSideSet,
  createQuote,
  quoteBuyCurrencySet
 } from '../../redux/actions';

import './PaymentFields.css'
import { Button, withStyles, TextField } from '@material-ui/core';
import Divider from '../../components/Divider/Divider'
import PaymentBalance from '../../components/PaymentBalance/PaymentBalance';
import PaymentVerticalDivider from '../../components/PaymentVerticalDivider/PaymentVerticalDivider';
import PaymentAmountInput from '../../components/PaymentAmountInput/PaymentAmountInput';
import EURCurrency from '../../components/EURCurrency/EURCurrency';
import { BigNumber } from 'bignumber.js';
import PaymentCurrency from '../../components/PaymentCurrency/PaymentCurrency';

const styles = theme => ({
  container: {
    width: '100vw',
    textAlign: 'center'
  },
  button: {
    margin: theme.spacing.unit,
  },
  textField: {
    marginLeft: -4,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 0,
    padding: 0,
    height: '100%',
    width: '100%'
  }
});

const { BUY, SELL} = QUOTE_TYPE
const BUY_VOLUME_INPUT_ID = 'buyVolume';
const SELL_VOLUME_INPUT_ID = 'sellVolume';

const FIXED_SIDE_PROCESS = {
  BUY: {
    storeSetter: 'quoteBuyVolumeSet',
    stateValue: BUY_VOLUME_INPUT_ID,
    unfocusedStateValue: SELL_VOLUME_INPUT_ID,
    unfocusedValueOperation: 'sellValue'
  },
  SELL: {
    storeSetter: 'quoteSellVolumeSet',
    stateValue: SELL_VOLUME_INPUT_ID,
    unfocusedStateValue:BUY_VOLUME_INPUT_ID,
    unfocusedValueOperation: 'buyValue'
  }
}

const FIXED_SIDE_ACTION = {
  [BUY_VOLUME_INPUT_ID]: BUY,
  [SELL_VOLUME_INPUT_ID]: SELL
}

const POLL_INTERVAL = `${process.env.REACT_APP_POLL_INTERVAL}`;

// TODO - this needs to be split in more components
// TODO - this screams for refactoring, move EVERYTHING to clean actions
class PaymentFields extends Component {
    state = {
      focused: SELL_VOLUME_INPUT_ID,
      sellVolume: '1',
      buyVolume: '0',
      valid: {
        sellVolume: true,
        buyVolume: true,
        sellVolumeExceedsBalance: false
      }
    };

    onSubmit = event => {
      event.preventDefault()
      this.onConfirm()
    };

    onConfirm = () => {
      this.props.onConfirm(this.state.name, this.state.address, this.state.currency)
    };
    
    focusBuyInputField = input => {
      input && input.focus();
    };
        
    handleFocus = (focused) => {
      this.props.quoteFixedSideSet(FIXED_SIDE_ACTION[focused])
      this.setState({
        focused
      })
    }

    checkAmountIsLessThanBalance() {
      if (!this.sellValue()) return;
      const sellBigNumber = new BigNumber(this.sellValue())
      const exceedsBalance = sellBigNumber.isNaN() || sellBigNumber.isGreaterThan(new BigNumber(this.props.balance));
      const { valid } = { ...this.state}
      valid.sellVolumeExceedsBalance = exceedsBalance;
      this.setState({
        ...this.state,
        valid
      });
    }

    validateAmounts = () => {
      debounce(() => this.checkAmountIsLessThanBalance(), 200)()
    }
    
    handleChange = (name, skipValidityCheck) => async event => {
      const value = event.target.value;
      let valid = {...this.state.valid}
      valid[name] = skipValidityCheck || event.target.checkValidity();
      this.setState({
        [name]: value,
        valid
      });
      this.validateAmounts();
      if(!this.quotePollingActivated()) {
        debounce(async () => {
          this.updateUnfocusedSideState();
          await this.triggerQuoteCreate();
        }, 500)()  
      }
    };

    quotePollingActivated() {
      return POLL_INTERVAL > 0;
    }

    formIsValid = () => {
      return this.sellValue() 
        && this.state.valid.buyVolume
        && !this.props.apiError
        && !this.state.valid.sellVolumeExceedsBalance;
    }

    renderBuyVolumeCurrency = () => {
      return <PaymentCurrency label={this.props.buy_currency}></PaymentCurrency>
    }

    async componentDidMount() {
      await this.props.getBeneficiary(this.props.beneficiaryId);
      await this.props.quoteBeneficiarySet(this.props.beneficiary.id)
      await this.props.quoteBuyCurrencySet(this.props.beneficiary.crypto_currency)
      await this.props.quoteFixedSideSet(SELL);
      await this.props.quoteSellVolumeSet(this.state.sellVolume);
      await this.createQuote()
      if (this.quotePollingActivated()) {
        this.timer = setInterval(async () => {
          await this.triggerQuoteCreate()
        }, POLL_INTERVAL);
      }
    }

    componentWillUnmount() {
      if (this.timer) {
        clearInterval(this.timer)
        this.timer = null;
      }
    }

    async createQuote() {
      await this.props.createQuote(
        this.props.beneficiary_id,
        this.props.buy_currency,
        this.props.sell_currency,
        this.props.fixed_side,
        this.props.buy_volume,
        this.props.sell_volume
      )
    }

    async triggerQuoteCreate() {
      const focusedSideData = FIXED_SIDE_PROCESS[this.props.fixed_side];
      const valueToQuoteOn = this.state[focusedSideData.stateValue];
      if (valueToQuoteOn === '') return
      await this.props[focusedSideData.storeSetter](valueToQuoteOn);
      await this.createQuote();
      this.validateAmounts();
      this.props.quoteIsValidSet(this.formIsValid())
    }

    updateUnfocusedSideState() {
      const processedAmount = this[FIXED_SIDE_PROCESS[this.props.fixed_side].unfocusedValueOperation]();
      this.setState({
        [FIXED_SIDE_PROCESS[this.props.fixed_side].unfocusedStateValue]: processedAmount
      })
    }

    buyValue() {
      return this.props.fixed_side === BUY
      ? this.state.buyVolume 
      : this.props.buy_volume
    }
  
    sellValue() {
      const value = this.props.fixed_side === SELL
      ? this.state.sellVolume
      : this.props.sell_volume
      return value
    }

    setBuyToAvailableBalance() {
      const { valid } = { ...this.state}
      valid.sellVolume = true;      
      this.setState({
        sellVolume: this.props.balance,
        valid
      })
      this.handleFocus(SELL_VOLUME_INPUT_ID)
    }

    availableBuyVolumeWithoutSettlementFee() {
      return parseFloat((this.props.balance - parseFloat(this.settlementFeeInEUR())) / this.props.price).toFixed(5)
    }

    settlementFeeInEUR() {
      return parseFloat(this.props.settlement_fee * this.props.price).toFixed(2);
    }
  
    render() {
      const { classes } = this.props;
      const inputProps = {
        step: 'any',
        min: 0,
        pattern: '\d*'
      };

      return (
        <div className={classes.container}>
          
          <PaymentBalance title="Available balance:" onClick={() => this.setBuyToAvailableBalance() }>
            €{ this.props.balance }
          </PaymentBalance>
          <PaymentVerticalDivider></PaymentVerticalDivider>
          <form onSubmit={this.onSubmit}>
            <PaymentAmountInput>
              <EURCurrency></EURCurrency>
              <TextField
                id="saleVolume"
                type="number"
                disabled={this.state.focused === BUY_VOLUME_INPUT_ID}
                inputProps={inputProps}
                className={`${classes.textField} ${!this.formIsValid() ? 'ERROR': ''}`}
                placeholder="Sell"
                margin="normal"
                variant="outlined"
                value={this.sellValue()}
                onChange={this.handleChange(SELL_VOLUME_INPUT_ID)}
                error={!this.state.valid.sellVolume || this.state.valid.sellVolumeExceedsBalance}
                onFocus={() => this.handleFocus(SELL_VOLUME_INPUT_ID)}
                onClick={() => {
                  this.setState({sellVolume: this.props.sell_volume})
                  this.handleFocus(SELL_VOLUME_INPUT_ID)}
                }
                required
              />
            </PaymentAmountInput>
            <PaymentVerticalDivider></PaymentVerticalDivider>
            <PaymentBalance title="Price:">
              1 {this.props.buy_currency} = €{ parseFloat(this.props.price).toFixed(2) }
            </PaymentBalance>
            <PaymentVerticalDivider></PaymentVerticalDivider>
            <PaymentBalance title="Settlement fee:">
              {this.props.buy_currency} { parseFloat(this.props.settlement_fee).toFixed(5) } (~ { this.props.sell_currency } { this.settlementFeeInEUR() })
            </PaymentBalance>
            <PaymentVerticalDivider></PaymentVerticalDivider>
            <PaymentAmountInput>
              {this.renderBuyVolumeCurrency()}
              <TextField
                id="buyVolume"
                type="number"
                disabled={this.state.focused === SELL_VOLUME_INPUT_ID}
                inputProps={inputProps}
                className={classes.textField}
                placeholder="Buy"
                margin="normal"
                variant="outlined"
                value={this.buyValue()}
                onChange={this.handleChange(BUY_VOLUME_INPUT_ID)}
                error={!this.state.valid.buyVolume}
                onFocus={() => this.handleFocus(BUY_VOLUME_INPUT_ID)} 
                onClick={() => {
                  this.setState({buyVolume: this.props.buy_volume})
                  this.handleFocus(BUY_VOLUME_INPUT_ID)}
                }
                required
              />
            </PaymentAmountInput>
            <PaymentVerticalDivider></PaymentVerticalDivider>
            <PaymentBalance title="Quote Status:">
              <div className={`${this.props.status}`}>{this.props.status === 'PROCESSING' ? 'Updating' : this.props.status === 'SUCCESS' ? 'Live' : 'Retrying' }</div>
            </PaymentBalance>
            <Divider size="3" unit="vh"></Divider>
            <Button className={classes.button}
                    type="submit"
                    disabled={!this.formIsValid()}
                    variant="contained" 
                    color="primary">
              Send
            </Button>
            {/* <div className="ERROR">{this.settlementFeeInEUR() > this.props.balance ? 'Insufficient funds, please topup!': '' }</div> */}
            <div className="ERROR">{this.state.valid.sellVolumeExceedsBalance ? 'Buy volume exceeds available balance!': '' }</div>
            <div className="ERROR">{this.props.apiError ? this.props.apiError : '' }</div>
          </form>
        </div>
      );
    }
  }

PaymentFields.propTypes = {
  classes: PropTypes.object.isRequired,
  onConfirm: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  const { available, balance } = state.account;
  const { beneficiary } = state.beneficiaries;
  const { 
    quoteIsValid,
    status,
    apiError,
   } = state.trades;
  
   const { 
    beneficiary_id,
    sell_volume,
    buy_volume,
    fixed_side,
    buy_currency,
    sell_currency,
    price,
    settlement_fee,
   } = state.trades.quote;
  
   return { 
    beneficiary,
    available, balance,
    quoteIsValid,
    status,
    apiError,
    beneficiary_id,
    sell_volume,
    buy_volume,
    fixed_side,
    buy_currency,
    sell_currency,
    price,
    settlement_fee,
  };
};

const mapDispatchToProps = { 
  quoteIsValidSet,
  getBeneficiary,
  quoteBeneficiarySet,
  quoteSellVolumeSet,
  quoteBuyVolumeSet,
  quoteFixedSideSet,
  createQuote,
  quoteBuyCurrencySet
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(PaymentFields));
