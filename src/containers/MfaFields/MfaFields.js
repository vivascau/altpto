import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './MfaFields.css'
import { TextField, Button, withStyles } from '@material-ui/core';
import Divider from '../../components/Divider/Divider';

const styles = theme => ({
  container: {
    width: '100vw',
    textAlign: 'center'
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    width: '280px'
  }
});

class MfaFields extends Component {
    state = {
      mfaCode: '',
      valid: {
        mfaCode: true
      }
    };

    onSubmit = event => {
      event.preventDefault()
      this.onConfirm()
    };

    onConfirm = () => {
      this.props.onConfirm(this.state.mfaCode)
    };

    handleChange = name => event => {
      let valid = {...this.state.valid}
      valid[name] = event.target.checkValidity();
      this.setState({
        [name]: event.target.value,
        valid
      });
    };

    formNotValid = () => {
      return !this.state.mfaCode
    };

    render() {
      const { classes } = this.props;
      return (
        <div className={classes.container}>
          <form onSubmit={this.onSubmit}>
            <div>
              <TextField
                className={classes.input}
                id="mfaCode"
                value={this.state.mfaCode}
                onChange={this.handleChange('mfaCode')}
                error={!this.state.valid.mfaCode}
                label="MFA Code"
                type="number"
                required
                margin="normal"
              />
            </div>
            <Divider size="32"></Divider>
            <Button className={classes.button}
                    type="submit"
                    disabled={this.formNotValid()}
                    variant="contained" 
                    color="primary">
              Login
            </Button>
          </form>
        </div>
      );
    }
  }

MfaFields.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MfaFields);
