import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import { DialogContent, DialogActions, Button } from '@material-ui/core';
import debounce from 'lodash.debounce';

import { connect } from 'react-redux';
import { quoteOpenTradeSet, createTrade } from '../../redux/actions';

import Divider from '../../components/Divider/Divider';

class PaymentProcessingDialog extends Component {
  state = {
    redirect: false
  }

  handleClose = () => {
    this.props.onClose(this.props);
  };
  
  handleConfirm = async () => {
    await this.props.createTrade(
      this.props.quote_id,
      this.props.beneficiary_id,
      this.props.buy_currency,
      this.props.buy_volume,
      this.props.sell_currency,
      this.props.sell_volume,
      this.props.settlement_fee,
      this.props.price
    )
    if (this.props.trade) {
      debounce(() => {
        this.props.quoteOpenTradeSet(false);
        this.setState({
          redirect: true
        });
      }, 3000)()
    }
  };

  renderRedirectToTransactions() {
    if (this.state.redirect) {
      return <Redirect to={`/transactions/${this.props.trade}`}></Redirect>
    }
  }

  renderDialogContent() {
    if (this.props.trade && typeof this.props.trade === 'string') {
      return (
        <DialogContent>
          <Divider size="20"></Divider>
          <div>You have successfully created the trade with id:</div>
          <div><b>{ this.props.trade }</b></div>
          <Divider size="20"></Divider>
          <div>You will now be taken to transaction details.</div>
          <Divider size="20"></Divider>
        </DialogContent>)
    }
    return (
      <DialogContent>
        <div>By pressing 'CONFIRM' you instruct Altalix to execute the following transaction on your behalf:</div>
        <Divider size="20"></Divider>
        <div className="PaymentProcessingDialog-info"><b>You send:</b><div>{ this.props.sell_currency } { parseFloat(this.props.sell_volume) }</div></div>
        <Divider size="8"></Divider>
        <div className="PaymentProcessingDialog-info"><b>They receive:</b><div>{ this.props.buy_currency } { parseFloat(this.props.buy_volume) }</div></div>
        <Divider size="8"></Divider>
        <div className="PaymentProcessingDialog-info"><b>Settlement fee:</b><div>{ this.props.buy_currency } { parseFloat(this.props.settlement_fee).toFixed(5) } (~ {this.props.sell_currency} {parseFloat(this.props.settlement_fee * this.props.price).toFixed(2)})</div></div>
        <Divider size="8"></Divider>
        <div className="PaymentProcessingDialog-info"><b>Your rate:</b><div>{ this.props.buy_currency } 1 = € { parseFloat(this.props.price).toFixed(2) }</div></div>
      </DialogContent>)
  }

  renderDialogActions() {
    if (!this.props.trade) {
      return (
        <DialogActions>
          <Button onClick={this.handleClose} variant="outlined" color="primary">
            Cancel
          </Button>
          <Button onClick={this.handleConfirm}
                  variant="contained" 
                  color="primary"
                  autoFocus
                  disabled={!this.props.quoteIsValid}>
            CONFIRM
          </Button>
        </DialogActions>)
    }
  }
  
  render() {
    const { onClose, open } = this.props;

    return (
      <Dialog onClose={onClose} aria-labelledby="simple-dialog-title" open={open}>
        <DialogTitle id="simple-dialog-title">Execute Payment</DialogTitle>
        { this.renderDialogContent() }
        { this.renderDialogActions() }
        { this.renderRedirectToTransactions() }
      </Dialog>
    );
  }
}

PaymentProcessingDialog.propTypes = {
  onClose: PropTypes.func,
};

const mapStateToProps = state => {
  const { beneficiary } = state.beneficiaries;
  const { 
    quoteIsValid,
    status,
    trade
   } = state.trades;
  
   const {
    id,
    beneficiary_id, 
    sell_currency,
    sell_volume,
    buy_currency,
    buy_volume,
    settlement_fee,
    price,
   } = state.trades.quote;
  
   return { 
    beneficiary,
    quoteIsValid,
    status,
    trade,
    quote_id: id,
    beneficiary_id, 
    sell_currency,
    sell_volume,
    buy_currency,
    buy_volume,
    settlement_fee,
    price,
  };
};
const mapDispatchToProps = { 
  quoteOpenTradeSet,
  createTrade
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentProcessingDialog);