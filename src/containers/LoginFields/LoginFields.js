import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './LoginFields.css'
import { TextField, Button, withStyles } from '@material-ui/core';
import Divider from '../../components/Divider/Divider';

const styles = theme => ({
  container: {
    width: '100vw',
    textAlign: 'center'
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    width: '280px'
  }
});

class LoginFields extends Component {
    state = {
      email: '',
      password: '',
      valid: {
        email: true,
        password: true
      }
    };

    onSubmit = event => {
      event.preventDefault()
      this.onLogin()
    };

    onLogin = () => {
      this.props.onLogin(this.state.email, this.state.password)
    };

    onFocus = () => {
      this.props.onFocus()
    }

    handleChange = name => event => {
      let valid = {...this.state.valid}
      valid[name] = event.target.checkValidity();
      this.setState({
        [name]: event.target.value,
        valid
      });
    };

    formNotValid = () => {
      return !this.state.email || !this.state.password
    }

    render() {
      const { classes } = this.props;
      return (
        <div className={classes.container}>
          <form onSubmit={this.onSubmit}>
            <div>
              <TextField
                className={classes.input}
                id="email"
                value={this.state.email}
                onChange={this.handleChange('email')}
                error={!this.state.valid.email}
                label="Email"
                type="email"
                required
                margin="normal"
                onFocus={this.onFocus}
              />
            </div>
            <div>
              <TextField
                className={classes.input}
                id="password"
                value={this.state.password}
                onChange={this.handleChange('password')}
                error={!this.state.valid.password}
                label="Password"
                type="password"
                required
                autoComplete="current-password"
                onFocus={this.onFocus}
              />
            </div>
            <Divider size="32"></Divider>
            <Button className={classes.button}
                    type="submit"
                    disabled={this.formNotValid()}
                    variant="contained" 
                    color="primary">
              Continue
            </Button>
          </form>
        </div>
      );
    }
  }

LoginFields.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LoginFields);
