import React, { Component } from 'react';

import './Settings.css';

import { connect } from 'react-redux';
import { onPageWithFooterAndBackButton, getUserAccount, logout } from '../../redux/actions';

import PageContent from 'components/PageContent/PageContent';
import AccountBalance from 'components/AccountBalance/AccountBalance';
import Info from 'components/Info/Info';
import Divider from 'components/Divider/Divider';
import UserProfile from 'components/UserProfile/UserProfile';
import ActionButton from 'components/ActionButton/ActionButton';

class Settings extends Component {  
  renderBalance() {
    return <AccountBalance amount={this.props.balance} flatBottom></AccountBalance>
  }

  onLogout() {
    this.props.logout()
    this.props.history.push('/login')
  }

  async componentWillMount() {
    this.props.onPageWithFooterAndBackButton()
    await this.props.getUserAccount();
  }

  renderUserProfile() {
    return (<UserProfile onLogout={() => this.onLogout()}></UserProfile>)
  }

  render() {
    const { firstName, lastName, email } = this.props;
    return (
      <PageContent>
        { this.renderUserProfile() }
        <Info title="" withBorder>
          <Info title="Name">
            <span>{ firstName } { lastName }</span>
          </Info>
          <Divider size="3" unit="vh"></Divider>
          <Info title="Email">
            <span>{ email }</span>
          </Info>
        </Info>
        <Divider size="3" unit="vh"></Divider>
        <a href="mailto:info@altalix.com?Subject=Change details for my account">
          <ActionButton>
            Email us for any changes
          </ActionButton>
        </a>
      </PageContent>
    )
  };
}

const mapStateToProps = state => {
  const { firstName, lastName, email } = state.account;
  return { firstName, lastName, email };
};
const mapDispatchToProps = { onPageWithFooterAndBackButton, getUserAccount, logout };

export default connect(mapStateToProps, mapDispatchToProps)(Settings);