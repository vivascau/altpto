import React, { Component } from 'react';

import './TransactionDetails.css';

import { connect } from 'react-redux';
import { onPageWithFooterAndBackButton, getTrade, getBeneficiary } from '../../redux/actions';

import PageContent from '../../components/PageContent/PageContent';
import AccountCard from '../../components/AccountCard/AccountCard';
import Info from '../../components/Info/Info';
import Divider from '../../components/Divider/Divider';
import { Grid } from '@material-ui/core';
import TransactionHistoryStatus from '../../components/TransactionHistoryStatus/TransactionHistoryStatus';
import TransactionHistoryUpdate from '../../components/TransactionHistoryUpdate/TransactionHistoryUpdate';

const TRANSACTION_POLL_INTERVAL = `${process.env.REACT_APP_TRANSACTION_POLL_INTERVAL}`;

class TransactionDetails extends Component {
  state = {
    info: {
      title: 'Details Regarding your Settlement',
      content: 'Settlement time will vary depend on currency being settled. Please allow up to a few hours for the network to settle.'
    },
    account: {
      name: 'ALTX-32',
      balance: '0.262',
      unit: 'BCH',
      pricePerUnit: '5619.61',
      conversionUnit: '€'
    },
    transaction: {
      walletName: 'wallet 1',
      status: 'pending',
      type: 'outgoing',
      amount: 13,
      currency: 'ETH',
      history: [
        { 
          time: '', 
          update: 'Executed'
        }
      ]
    }
  };

  async componentWillMount() {
    this.props.onPageWithFooterAndBackButton();
    await this.triggerGetTrade();
    await this.props.getBeneficiary(this.props.trade.beneficiary_id)
    if (this.tradePollingActivated()) {
      this.timer = setInterval(async () => {
        await this.triggerGetTrade();
      }, TRANSACTION_POLL_INTERVAL);
    }
  }

  async triggerGetTrade() {
    await this.props.getTrade(this.props.match.params.id);
  }

  tradePollingActivated() {
    return TRANSACTION_POLL_INTERVAL > 0;
  }

  renderHistoryItemDetails() {
    const { trade } = this.props;
    if (!trade) { return; }
    return (<Grid
      container
      direction="column"
      justify="center"
      alignItems="center">
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center">
            <div className="TransactionDetails-account TransactionDetails-label">They receive:</div>
            <div className="TransactionDetails-account">{ trade.buy_currency } { trade.buy_volume && parseFloat(trade.buy_volume) }</div>
        </Grid>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center">
          <div className="TransactionDetails-account TransactionDetails-label">You sent:</div>
          <div className="TransactionDetails-account">€ { trade.sell_volume && parseFloat(trade.sell_volume) }</div>
        </Grid>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center">
          <div className="TransactionDetails-account TransactionDetails-label">Settlement fee:</div>
          <div className="TransactionDetails-account">{trade.buy_currency} { trade.settlement_fee && parseFloat(trade.settlement_fee) }</div>
          <div className="TransactionDetails-account">(~ € {parseFloat(trade.settlement_fee * trade.price).toFixed(2)})</div>
        </Grid>
      </Grid>)
  }

  renderHistoryItemPrice() {
    const { trade } = this.props;
    if (!trade) {return;}
    return (<Grid
      container
      direction="row"
      justify="center"
      alignItems="center">
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center">
          <div className="TransactionDetails-conversion">€</div>
          <div className="TransactionDetails-conversion">{ trade.price }</div>
          <div className="TransactionDetails-conversion">=</div>
          <div className="TransactionDetails-conversion">{ trade.buy_currency }</div>
          <div className="TransactionDetails-conversion">1</div>
      </Grid>
    </Grid>);
  }

  renderTransactionHistory = () => {
    const { transaction } = this.state;
    const { history } = transaction;
    const { trade } = this.props;
    if (!trade) { return; }
    return (
      <Grid 
        container
        direction="column"
        justify="center"
        alignItems="center">
          <Divider size="5" unit="vh"></Divider>
          <TransactionHistoryStatus status={ trade.status }></TransactionHistoryStatus>
          <Divider size="2"></Divider>
          { history.map((historyItem, i) =>
              <div key={i}>
                <TransactionHistoryUpdate 
                  update={ historyItem.update }
                  time={new Date(trade.executed_datetime).getTime()}>
                </TransactionHistoryUpdate>
                <Divider size="2"></Divider>
              </div>) }
      </Grid>
    )
  }

  // TODO - split in small components
  render() {
    return (
      <PageContent>
        <Info title={this.state.info.title}>
          { this.state.info.content }
        </Info>
        <Divider size="8"></Divider>
        <AccountCard title="Sent to">
        <div className="TransactionDetails-account-name">{ this.props.beneficiary.display_name }</div>
        </AccountCard>
        <Divider size="8"></Divider>
        <AccountCard title="">
        { this.renderHistoryItemDetails() }
        { this.renderHistoryItemPrice() }
        </AccountCard>
        { this.renderTransactionHistory()}
      </PageContent>
    )
  };
}

const mapStateToProps = state => {
  const { available, balance } = state.account;
  const { trade } = state.trades;
  const { beneficiary } = state.beneficiaries;
  return { available, balance, trade, beneficiary };
};
const mapDispatchToProps = { onPageWithFooterAndBackButton, getTrade, getBeneficiary };

export default connect(mapStateToProps, mapDispatchToProps)(TransactionDetails);