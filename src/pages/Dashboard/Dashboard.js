import React, { Component } from 'react';

import './Dashboard.css';

import { connect } from 'react-redux';
import { onPageWithFooterAndBackButton, getUserAccount, getTrades, logout } from '../../redux/actions';

import PageContent from '../../components/PageContent/PageContent';
import AccountBalance from '../../components/AccountBalance/AccountBalance';
import AccountAvailable from '../../components/AccountAvailable/AccountAvailable';
import Info from '../../components/Info/Info';
import QuickActions from '../../containers/QuickActions/QuickActions';
import Divider from '../../components/Divider/Divider';

class Dashboard extends Component {  
  renderBalance() {
    return <AccountBalance amount={this.props.balance} flatBottom></AccountBalance>
  }

  onLogout() {
    this.props.logout()
    this.props.history.push('/login')
  }

  async componentWillMount() {
    this.props.onPageWithFooterAndBackButton()
    await this.props.getUserAccount();
    await this.props.getTrades();
  }
  renderAvaialableBalance() {
    if (this.props.available !== this.props.balance ) {
      return (<AccountAvailable availableBalance={this.props.available}></AccountAvailable>)
    }
  }
  
  renderUpdates() {
    const pendingTrades = this.props.trades.filter(trade => {
      return trade.status === 'PENDING'
    })
    const pendingSize = pendingTrades.length
    if (pendingSize > 0) {
      return (
        <Info title="Updates" withBorder>
          <span>You have { pendingSize } trade{ pendingSize > 1 ? 's' : '' } currently ‘pending’ please visit your Transactions history to view more information</span>
        </Info>
      )
    }

    return (
      <Info title="News" withBorder>
        <span>You can now start a new payment by using the quick action below.</span>
      </Info>)
  }
  render() {
    return (
      <PageContent>
        { this.renderBalance() }
        <Divider></Divider>
          { this.renderAvaialableBalance() }
        <Divider size="10" unit="vh"></Divider>
        { this.renderUpdates() }
        <Divider size="5" unit="vh"></Divider>
        <QuickActions></QuickActions>
      </PageContent>
    )
  };
}

const mapStateToProps = state => {
  const { available, balance } = state.account;
  const { trades } = state.trades;
  return { available, balance, trades };
};
const mapDispatchToProps = { onPageWithFooterAndBackButton, getUserAccount, getTrades, logout };

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);