import React, { Component } from 'react';
import { Grid } from '@material-ui/core';

import './Addresses.css';

import { connect } from 'react-redux';
import { onPageWithFooterAndBackButton, getAllBeneficiaries } from '../../redux/actions';

import PageContent from 'components/PageContent/PageContent';
import AddressSearchBar from 'components/AddressSearchBar/AddressSearchBar';
import AddressItem from 'components/AddressItem/AddressItem';

class Addresses extends Component {
  state = {
    addresses: []
  }

  newAddressPressed = () => {
    this.props.history.push('/addresses/new')
  }

  addressDetails = (id) => {
    this.props.history.push(`/addresses/${id}`)
  }

  async componentDidMount() {
    this.props.onPageWithFooterAndBackButton()
    await this.props.getAllBeneficiaries()
    const addresses = this.props.beneficiaries;
    this.setState({
      addresses
    })
  }

  renderAdressItems = () => {
    if (this.state.addresses.length > 0) {
      return this.state.addresses.map(({ 
        id,
        display_name,
        name,
        crypto_address,
        crypto_currency,
        status
        }) => <AddressItem
              onClick={this.addressDetails}
              key={id}
              walletName={display_name} 
              contactName={name} 
              address={crypto_address}
              currency={crypto_currency}
              id={id}
              status={status}>
            </AddressItem>)
    } else {
      return (<div>None</div>)
    }
  }

  onSearchChange(ev) {
    const { value } = ev.target;
    const addresses = this.props.beneficiaries.filter(address => {
      return address.display_name.toLowerCase().search(value.toLowerCase()) !== -1 
             || address.crypto_address.toLowerCase().search(value.toLowerCase()) !== -1
             || address.crypto_currency.toLowerCase().search(value.toLowerCase()) !== -1
      }
    )
    this.setState({addresses})
  }

  renderAddresses = () => {
    return (
      <Grid
        container
        direction="column"
        alignContent="stretch"
        justify="center"
        alignItems="center"
      >
        <AddressSearchBar onChange={ (ev) => this.onSearchChange(ev) } placeholder="Search by name, coin or address "></AddressSearchBar>     
        { this.renderAdressItems() }
        
      </Grid>
    )
  }

  render() {
    return (
      <PageContent>
        { this.renderAddresses() }
        <div className="Addresses-new-container">
          <button className="mdc-fab Addresses-new" aria-label="New Address" onClick={this.newAddressPressed}>
            <span className="mdc-fab__icon">+</span>
          </button>
        </div>
      </PageContent>
    )
  };
}

const mapStateToProps = state => {
  const { beneficiaries } = state.beneficiaries;
  return { beneficiaries };
};
const mapDispatchToProps = { onPageWithFooterAndBackButton, getAllBeneficiaries };
export default connect(mapStateToProps, mapDispatchToProps)(Addresses);