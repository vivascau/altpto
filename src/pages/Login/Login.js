import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setEmail, setPassword, clearLoginError, hideFooter, withoutBackButton } from '../../redux/actions';

import './Login.css';
import LoginFields from '../../containers/LoginFields/LoginFields';
import PageContent from '../../components/PageContent/PageContent';
import Divider from '../../components/Divider/Divider';
import AppLogo from '../../components/AppLogo/AppLogo';

const Login = class Login extends Component {
  componentWillMount() {
    this.props.hideFooter()
    this.props.withoutBackButton()
  }

  onLogin = (username, password) => {
    this.props.setEmail(username)
    this.props.setPassword(password)

    this.props.history.push('/mfa')
  }
  
  onFocus = () => {
    this.props.clearLoginError()
  }

  renderError() {
    if (this.props.error) {
      return(
        <div className="Login-error">
          {this.props.status} - {this.props.error}
        </div>
      )
    }
  }

  render() {
    return (
      <PageContent>
        <Divider size="5" unit="vh"></Divider>
        <AppLogo></AppLogo>
        <LoginFields onLogin={this.onLogin} onFocus={this.onFocus}></LoginFields>
        {this.renderError()}
      </PageContent>
    )
  }
}

const mapDispatchToProps = { setEmail, setPassword, clearLoginError, hideFooter, withoutBackButton };
const mapStateToProps = state => {
  const { email, password, status, error } = state.auth;
  return { email, password, status, error };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);