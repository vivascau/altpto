import React, { Component } from 'react';
import { connect } from 'react-redux';
import { hideFooter, getUserAccount, quoteOpenTradeSet, createTradeResponse } from '../../redux/actions';

import './Payment.css';

import PageContent from 'components/PageContent/PageContent';
import PaymentFields from 'containers/PaymentFields/PaymentFields';
import PaymentProcessingDialog from '../../containers/PaymentProcessingDialog/PaymentProcessingDialog';
import { Grid } from '@material-ui/core';

class Payment extends Component {

  sendPayment = () => {
    this.handleClickOpen()
  }

  changeContact = () => {
    this.props.history.push('/payees')
  }

  handleClickOpen = () => {
    this.props.quoteOpenTradeSet(true);
  };

  handleClose = () => {
    this.props.quoteOpenTradeSet(false);
  };
  
  async componentDidMount() {
    // await this.props.getUserAccount();
    this.props.hideFooter()
    await this.props.createTradeResponse(null)
  }

  render() {
    return (
      <PageContent className="Payment">  
        <Grid container
              onClick={this.changeContact}
              className="Payment-payee pointer"
              direction="row"
              justify="space-around"
              alignItems="center">
          <div>To:</div>
          <div className="Payment-payee-name">{this.props.beneficiary.display_name}</div>
        </Grid>
    
        <div className="Payment-container">
          <PaymentFields 
            onConfirm={this.sendPayment} 
            balance={this.props.available}
            beneficiaryId={this.props.match.params.id}>
          </PaymentFields>
        </div>
        
        <PaymentProcessingDialog
          open={this.props.openTrade}
          onClose={this.handleClose}
        />
      </PageContent>
    )
  };
}

const mapStateToProps = state => {
  const { beneficiary } = state.beneficiaries;
  const { available, balance } = state.account;
  const { 
    quoteIsValid,
    openTrade,
    quote
   } = state.trades;
  
   return { 
    beneficiary,
    available, balance,
    quoteIsValid,
    openTrade,
    quote
  };
};
const mapDispatchToProps = { 
  hideFooter,
  getUserAccount,
  quoteOpenTradeSet,
  createTradeResponse
};

export default connect(mapStateToProps, mapDispatchToProps)(Payment);