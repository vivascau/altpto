import React, { Component } from 'react';
import { Grid } from '@material-ui/core';

import { connect } from 'react-redux';
import { onPageWithFooterAndBackButton, getTrades, getAllBeneficiaries } from '../../redux/actions';

import './Transactions.css';

import PageContent from 'components/PageContent/PageContent';
import AccountBalance from 'components/AccountBalance/AccountBalance';
import TransactionHistoryTime from 'components/TransactionHistoryTime/TransactionHistoryTime';
import TransactionHistoryItem from 'components/TransactionHistoryItem/TransactionHistoryItem';
import Info from '../../components/Info/Info';

class Transactions extends Component {
  async componentWillMount() {
    this.props.onPageWithFooterAndBackButton()
    await this.props.getAllBeneficiaries()
    await this.props.getTrades();
  }

  newTransactionPressed = () => {
    this.props.history.push('/payees')
  }

  viewTradeDetails = (id) => {
    this.props.history.push(`/transactions/${id}`)
  }

  renderTransactionItems = () => {
    const { trades } = this.props;
    return trades.map((trade) =>
      <TransactionHistoryItem
        onClick={this.viewTradeDetails}
        key={trade.id}
        tradeId={trade.id}
        walletName={this.props.beneficiariesToWalletMap[trade.beneficiary_id]} 
        status={trade.status} 
        amount={parseFloat(trade.buy_volume)}
        sellVolume={parseFloat(trade.sell_volume)}
        currency={trade.buy_currency}>
      </TransactionHistoryItem>)
  }

  renderTransactionsHistory = () => {
    const { trades } = this.props;
    if (!trades || !trades.length) return (<div className="Transactions-none">There are no transactions to display.</div>);
    const lastTransactionDate = new Date(trades[0].executed_datetime)
    const month = lastTransactionDate && lastTransactionDate.toLocaleString('en-uk', { month: 'long' })
    const year = lastTransactionDate && lastTransactionDate.getFullYear()

    return (
      <Grid
        container
        direction="column"
        justify="flex-start"
        alignItems="flex-start"
      >
        <TransactionHistoryTime time={`${month} ${year}`}></TransactionHistoryTime>
        { this.renderTransactionItems() }
        
      </Grid>
    )
  }

  render() {
    return (
      <PageContent>
        
        <AccountBalance amount={this.props.balance}></AccountBalance>
        
        <button className="mdc-fab Transactions-new" aria-label="New Transaction" onClick={this.newTransactionPressed}>
          <span className="mdc-fab__icon">+</span>
        </button>
        <Info withBorder>
          { this.renderTransactionsHistory() }
        </Info>
      </PageContent>
    )
  };
}

const mapStateToProps = state => {
  const { available, balance } = state.account;
  const { beneficiaries, beneficiariesToWalletMap } = state.beneficiaries
  const { trades } = state.trades;
  return { available, balance, trades, beneficiaries, beneficiariesToWalletMap };
};
const mapDispatchToProps = { onPageWithFooterAndBackButton, getTrades, getAllBeneficiaries };

export default connect(mapStateToProps, mapDispatchToProps)(Transactions);