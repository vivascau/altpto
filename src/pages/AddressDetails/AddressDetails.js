import React, { Component } from 'react';
import { Grid, Button } from '@material-ui/core';

import './AddressDetails.css';

import { connect } from 'react-redux';
import { onPageWithFooterAndBackButton, getBeneficiary } from '../../redux/actions';

import PageContent from 'components/PageContent/PageContent';
import AddressItem from 'components/AddressItem/AddressItem';
import Info from 'components/Info/Info';
import Divider from 'components/Divider/Divider';

class AddressDetails extends Component {
  newPaymentPressed = () => {
    this.props.history.push(`/payees/${this.props.beneficiary.id}/payment`)
  }

  async componentDidMount() {
    this.props.onPageWithFooterAndBackButton()
    await this.triggerGetBeneficiary()
  }

  async triggerGetBeneficiary() {
    await this.props.getBeneficiary(this.props.match.params.id);
  }

  renderAdressDetails = () => {
    const { beneficiary }  = this.props;
    return (<Grid
      container
      direction="column"
      alignContent="stretch"
      justify="center"
      alignItems="center"
    >
    <AddressItem
      type="negative"
      onClick={() => {}}
      walletName={ beneficiary.name }
      address={ `Account is ` + beneficiary.status}>
    </AddressItem>

    <Info title="Coin" withBorder>{ beneficiary.crypto_currency }</Info>
    <Divider size="16"></Divider>
    <Info title="Address" withBorder>{this.props.beneficiary.crypto_address}</Info>
    </Grid>)
  }

  renderAwaitingApproval() {
    if (this.isWaitingApproval()) {
      return (
        <div className="AddressDetails-awaiting-aproval">The current address is being verified - once it has been verified you will be able to make payments to this address.</div>
      )
    }
  }

  isWaitingApproval() {
    const { status } = this.props.beneficiary
    return status === 'PENDING';
  }

  render() {
    return (
      <PageContent>
        { this.renderAdressDetails() }
        <div className="AddressDetails-payment-container">
          { this.renderAwaitingApproval() }
          <Divider size="8"></Divider>
          <Button variant="contained" 
                  disabled={this.isWaitingApproval()}
                  color="primary"
                  className="AddressDetails-payment"
                  aria-label="New Payment"
                  onClick={this.newPaymentPressed}>
            Send money
          </Button>
        </div>
      </PageContent>
    )
  };
}

const mapStateToProps = state => {
  const { beneficiary } = state.beneficiaries;
  return { beneficiary };
};
const mapDispatchToProps = { onPageWithFooterAndBackButton, getBeneficiary };
export default connect(mapStateToProps, mapDispatchToProps)(AddressDetails);