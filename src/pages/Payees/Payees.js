import React, { Component } from 'react';
import { Grid } from '@material-ui/core';

import './Payees.css';

import { connect } from 'react-redux';
import { onPageWithFooterAndBackButton, getAllBeneficiaries } from '../../redux/actions';

import PageContent from 'components/PageContent/PageContent';
import AddressSearchBar from 'components/AddressSearchBar/AddressSearchBar';
import AddressItem from 'components/AddressItem/AddressItem';
import Info from 'components/Info/Info';

class Payees extends Component {
  state = {
    payees: []
  }

  newAddressPressed = () => {
    this.props.history.push('/addresses/new')
  }

  makePaymentToAddress = (id) => {
    this.props.history.push(`/payees/${id}/payment`)
  }
 
  async componentDidMount() {
    this.props.onPageWithFooterAndBackButton()
    await this.props.getAllBeneficiaries()
    const payees = this.props.approvedBeneficiaries;
    this.setState({
      payees
    })
  }

  renderAdressItems = () => {
    if (this.state.payees.length > 0) {
      return this.state.payees.map(({ 
        id,
        display_name,
        name,
        crypto_address,
        crypto_currency,
        status
        }) => <AddressItem
              onClick={this.makePaymentToAddress}
              key={id}
              walletName={display_name} 
              contactName={name} 
              address={crypto_address}
              currency={crypto_currency}
              id={id}
              status={status}>
            </AddressItem>)
    } else {
      return (<div>None</div>)
    }
  }

  onSearchChange(ev) {
    const { value } = ev.target;
    const payees = this.props.approvedBeneficiaries.filter(address => {
      return address.display_name.toLowerCase().search(value.toLowerCase()) !== -1 
             || address.crypto_address.toLowerCase().search(value.toLowerCase()) !== -1
             || address.crypto_currency.toLowerCase().search(value.toLowerCase()) !== -1
      }
    )
    this.setState({ payees })
  }

  renderAddresses = () => {
    return (
      <Grid
        container
        direction="column"
        alignContent="stretch"
        justify="center"
        alignItems="center"
      >
        <AddressSearchBar onChange={ (ev) => this.onSearchChange(ev) } placeholder="Search by name, coin or address "></AddressSearchBar>  
        { this.renderAdressItems() }
      </Grid>
    )
  }

  render() {
    return (
      <PageContent>
        <Info title="Chose a wallet">
          Chose a contact you want to make the payment to or add a new one by pressing the + button.
        </Info>
        
        { this.renderAddresses() }
        <div className="Payees-new-container">
          <button className="mdc-fab Payees-new" aria-label="New Payee" onClick={this.newAddressPressed}>
            <span className="mdc-fab__icon">+</span>
          </button>
        </div>
      </PageContent>
    )
  };
}

const mapStateToProps = state => {
  const { approvedBeneficiaries } = state.beneficiaries;
  return { approvedBeneficiaries };
};
const mapDispatchToProps = { onPageWithFooterAndBackButton, getAllBeneficiaries };
export default connect(mapStateToProps, mapDispatchToProps)(Payees);