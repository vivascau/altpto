import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import './404.css';

import Info from 'components/Info/Info';
import PageContent from 'components/PageContent/PageContent';
import Divider from '../../components/Divider/Divider';

export default class NotFound extends Component {
  render() {
    return (
        <PageContent>
          <Info withBorder>
            <div>404 - Page Not Found.</div>
          </Info>
          <Divider size="20"></Divider>
          <div className="NotFound-content">
            <Link to="/dashboard">Home</Link>
          </div>
        </PageContent>
    )
  }
}
