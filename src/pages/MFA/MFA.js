import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { setMFA, postLogin, getCurrentUser, getUserAccount, AUTH_LOGIN_STATE, hideFooter, withBackButton } from '../../redux/actions';
import './MFA.css';
import Info from '../../components/Info/Info';
import MfaFields from '../../containers/MfaFields/MfaFields';
import PageContent from '../../components/PageContent/PageContent';
import Divider from '../../components/Divider/Divider';

const MFA = class MFA extends Component {
  componentWillMount() {
    this.props.hideFooter()
    this.props.withBackButton()
  }

  onConfirm = async (mfaCode) => {
    this.props.setMFA(mfaCode)
    await this.props.postLogin()
    await this.props.getCurrentUser();
    await this.props.getUserAccount();
  }

  redirectIfError = () => {
    if (this.props.status === AUTH_LOGIN_STATE.ERROR) {
      return <Redirect to="/login"/>
    } else if (this.props.status === AUTH_LOGIN_STATE.SUCCESS) {
        return <Redirect to="/dashboard"/>
    }
  }

  render() {
    return (
        <PageContent>
          <Info title="Securing your session" withBorder>
            As part of logging in to every session, you will be required to authenticate using an MFA application like Google Authenticator. This enables us to maintain the highest level of security.
          </Info>
          <Divider size="5" unit="vh"></Divider>
          <MfaFields onConfirm={this.onConfirm}></MfaFields>
          {this.redirectIfError()}
        </PageContent>
    )
  }
}

const mapStateToProps = state => {
  const { status, error } = state.auth;
  return { status, error };
};
const mapDispatchToProps = { setMFA, postLogin, getCurrentUser, getUserAccount, hideFooter, withBackButton };

export default connect(mapStateToProps, mapDispatchToProps)(MFA);