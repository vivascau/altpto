import React, { Component } from 'react';

import { connect } from 'react-redux';
import { getCurrencies, createBeneficiary } from '../../redux/actions';
import './AddressesNew.css';

import PageContent from 'components/PageContent/PageContent';
import Info from 'components/Info/Info';
import NewContactFields from 'containers/NewContactFields/NewContactFields';

class AddressesNew extends Component {
  state = {
    contact: {
      name: '',
      address: '',
      currency: ''
    }
  }

  async componentWillMount() {
    await this.props.getCurrencies()
  }
  saveAddress = async (name, display_name, crypto_currency, crypto_address) => {
    await this.props.createBeneficiary(name, display_name, crypto_currency, crypto_address)
    this.props.history.push(`/addresses/${this.props.beneficiary.id}`)
  }

  render() {
    const { currencies } = this.props;
    if (!currencies) {
      return;
    }
    return (
      <PageContent className="AddressesNew">
        <Info title="Add new contact">
          Fill in address wallet details and press 'Confirm' button.
        </Info>
        
        <div className="AddressesNew-container">
          <NewContactFields onConfirm={this.saveAddress}
                            currencies={currencies}>
          </NewContactFields>
        </div>
      </PageContent>
    )
  };
}

const mapStateToProps = state => {
  const { beneficiary } = state.beneficiaries;
  const { currencies } = state.trades;
  return { beneficiary, currencies };
};
const mapDispatchToProps = { getCurrencies, createBeneficiary };

export default connect(mapStateToProps, mapDispatchToProps)(AddressesNew);