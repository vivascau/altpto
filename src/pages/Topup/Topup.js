import React, { Component } from 'react';

import './Topup.css';
import Info from 'components/Info/Info';
import PageContent from 'components/PageContent/PageContent';
import Divider from '../../components/Divider/Divider';
import TopupItem from '../../components/TopupItem/TopupItem';

export default class Topup extends Component {
  state = {
    accountName: 'GB33BUKB',
    iban: 'GB33BUKB20201555555555',
    bic: 'UKALTX-32',
    accountNumber: '3427462842384'
  }

  render() {
    return (
        <PageContent>
          <Info title="Sending Funds To Your Account" withBorder>
            In order to top up your account - please transfer funds to the following account 
            <div><b>(Funds must be sent from a bank account held in your name):</b></div>
          </Info>
          <Divider size="20"></Divider>
          <div className="Topup-content">
            <TopupItem title="AccountName">{ this.state.accountName }</TopupItem>
            <TopupItem title="IBAN">{ this.state.iban }</TopupItem>
            <TopupItem title="BIC">{ this.state.bic }</TopupItem>
            <TopupItem title="AccountNumber">{ this.state.accountNumber }</TopupItem>
          </div>
        </PageContent>
    )
  }
}
