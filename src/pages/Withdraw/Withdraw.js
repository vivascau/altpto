import React, { Component } from 'react';

import './Withdraw.css';
import Info from 'components/Info/Info';
import PageContent from 'components/PageContent/PageContent';
import Divider from 'components/Divider/Divider';
import ActionButton from 'components/ActionButton/ActionButton';

export default class Withdraw extends Component {
  render() {
    return (
        <PageContent>
          <Info title="Withdraw Funds From Your Account" withBorder>
            <div>You can also add one of your crypto accounts and transfer money to it.</div>
          </Info>
          <Divider size="20"></Divider>
          <a href="mailto:info@altalix.com?Subject=Withdraw money from my account">
            <ActionButton>
              Contact us for any help.
            </ActionButton>
          </a>
        </PageContent>
    )
  }
}
