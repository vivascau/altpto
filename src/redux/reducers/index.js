import { combineReducers } from 'redux';
import auth from './auth';
import account from './account';
import beneficiaries from './beneficiaries';
import trades from './trades'
import route from './route'

export default combineReducers({ auth , account, beneficiaries, trades, route });
