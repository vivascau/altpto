import { 
  API_CALL_ACTION,
  BENEFICIARIES_GET_ALL,
  BENEFICIARIES_GET_ONE,
  BENEFICIARIES_CREATE
} from '../actions/types';

const initialState = {
  beneficiaries: [],
  beneficiariesToWalletMap: {},
  approvedBeneficiaries: [],
  beneficiary: {},
  status: '',
  apiError: ''
};

const beneficiaries = (state = initialState, action) => {
  switch (action.type) {
    case BENEFICIARIES_GET_ALL: {
      const { beneficiaries } = action.payload
      const beneficiariesToWalletMap = beneficiaries.reduce((obj, beneficiary) => ({...obj, ...({[beneficiary.id]: `${ beneficiary.display_name }`})}), {})
      const approvedBeneficiaries = beneficiaries.filter( beneficiary => beneficiary.status !== 'PENDING' )
      return { ...state, beneficiaries, approvedBeneficiaries, beneficiariesToWalletMap };
    }
    case BENEFICIARIES_GET_ONE: {
      const { beneficiary } = action.payload
      return { ...state, beneficiary };
    }
    case BENEFICIARIES_CREATE: {
      const { beneficiary } = action.payload
      return { ...state, beneficiary };
    }
    case API_CALL_ACTION: {
      const { apiError, status } = action.payload
      return { ...state, apiError, status };
    }
    
    default: {
      return state ;
    }
  }
};

export default beneficiaries;
