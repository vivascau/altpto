import { 
  CURRENT_USER_GET,
  USER_ACCOUNT_GET,
  API_CALL_ACTION,
  AUTH_LOGOUT
} from '../actions/types';

const initialState = {
  userId: '',
  firstName: '',
  lastName: '',
  email: '',  
  available: '',
  balance: '',
  apiError: '',
  status: ''
};

const account = (state = initialState, action) => {
  switch (action.type) {
    case CURRENT_USER_GET: {
      const userId = action.payload.user.id
      const firstName = action.payload.user.first_name
      const lastName = action.payload.user.last_name
      const email = action.payload.user.email
      return { ...state, userId, firstName, lastName, email };
    }
    case USER_ACCOUNT_GET: {
      const { available, balance } = action.payload
      return { ...state, available, balance };
    }
    case API_CALL_ACTION: {
      const { apiError, status } = action.payload
      return { ...state, apiError, status };
    }
    case AUTH_LOGOUT: {
      return { ...initialState };
    }
    
    default: {
      return state ;
    }
  }
};

export default account;
