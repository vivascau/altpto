import { 
  SET_AUTH_EMAIL,
  SET_AUTH_PASSWORD,
  SET_AUTH_MFA,
  SET_AUTH_TOKEN,
  AUTH_LOGIN_POST,
  AUTH_LOGIN,
  API_CALL_ACTION
} from '../actionTypes';
import { AUTH_LOGOUT } from '../actions/types';

const initialState = {
  email: '',
  password: '',
  mfa: '',
  token: localStorage.getItem('TOKEN'),
  isLoggedIn: false,
  status: '',
  error: '',
  apiError: '',
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case SET_AUTH_EMAIL: {
      const { email } = action.payload
      return { ...state, email };
    }
    case SET_AUTH_PASSWORD: {
      const { password } = action.payload
      return { ...state, password };
    }
    case SET_AUTH_MFA: {
      const { mfa } = action.payload
      return { ...state, mfa };
    }
    case SET_AUTH_TOKEN: {
      const { token } = action.payload
      return { ...state, token };
    }
    case AUTH_LOGIN_POST: {
      const { error, status, isLoggedIn } = action.payload
      return { ...state, status, error, isLoggedIn };
    }
    case AUTH_LOGIN: {
      const { token } = action.payload
      return { ...state, token };
    }
    case AUTH_LOGOUT: {
      const token = null
      return { ...initialState, token };
    }
    case API_CALL_ACTION: {
      const { apiError } = action.payload
      return { ...state, apiError };
    }
    default: {
      return state ;
    }
  }
};

export default auth;
