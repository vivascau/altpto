import { 
  HEADER_TITLE_SET,
  HEADER_BACK_BUTTON_SET,
  FOOTER_VISIBLE_SET,
} from '../actions/types';

const initialState = {
  header: {
    title: 'Altalix',
    withBackButton: false
  },
  footer: {
    visible: true
  }
};

const route = (state = initialState, action) => {
  switch (action.type) {
    case HEADER_TITLE_SET: {
      const { title } = action.payload;
      return {...state, header: {...state.header, title } };
    }
    case HEADER_BACK_BUTTON_SET: {
      const { withBackButton } = action.payload;
      return {...state, header: {...state.header, withBackButton } };
    }
    case FOOTER_VISIBLE_SET: {
      const { visible } = action.payload;
      return {...state, footer: {...state.footer, visible } };
    }
    
    default: {
      return state;
    }
  }
};

export default route;
