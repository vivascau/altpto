import { 
  API_CALL_ACTION,
  QUOTES_CREATE,
  QUOTE_IS_VALID_SET,
  QUOTE_BENEFICIARY_SET,
  QUOTE_BUY_CURRENCY_SET,
  QUOTE_SELL_CURRENCY_SET,
  QUOTE_FIXED_SIDE_SET,
  QUOTE_BUY_VOLUME_SET,
  QUOTE_SELL_VOLUME_SET,
  QUOTE_OPEN_TRADE_SET,
  TRADES_CREATE,
  TRADES_GET,
  TRADE_DETAILS_GET,
  CURRENCIES_GET,
} from '../actions/types';

import { QUOTE_TYPE, FIXED_SELL_CURRENCY } from '../../utils/quotes';

const initialState = {
  quote: {
    id: '',
    beneficiary_id: '',
    buy_currency: '',
    sell_currency: FIXED_SELL_CURRENCY,
    fixed_side: QUOTE_TYPE.SELL,
    buy_volume: '',
    sell_volume: '1',
    price: '',
    settlement_fee: '',
    expiration_datetime: ''
  },
  trade: '',
  trades: [],
  currencies: [],
  quoteIsValid: false,
  openTrade: false,
  status: '',
  apiError: ''
};

const trades = (state = initialState, action) => {
  switch (action.type) {
    case API_CALL_ACTION: {
      const { apiError, status } = action.payload
      return { ...state, apiError, status };
    }
    case QUOTES_CREATE: {
      const { quote } = action.payload
      return { ...state, quote };
    }
    case QUOTE_IS_VALID_SET: {
      const { quoteIsValid } = action.payload
      return { ...state, quoteIsValid };
    }
    case QUOTE_BENEFICIARY_SET: {
      const { beneficiary_id } = action.payload
      const { quote } = {...state }
      quote.beneficiary_id = beneficiary_id
      return { ...state, quote };
    }
    case QUOTE_BUY_CURRENCY_SET: {
      const { buy_currency } = action.payload
      const { quote } = {...state }
      quote.buy_currency = buy_currency
      return { ...state, quote };
    }
    case QUOTE_SELL_CURRENCY_SET: {
      const { sell_currency } = action.payload
      const { quote } = {...state}
      quote.sell_currency = sell_currency;
      return { ...state, quote };
    }
    case QUOTE_FIXED_SIDE_SET: {
      const { fixed_side } = action.payload
      const { quote } = {...state}
      quote.fixed_side = fixed_side;
      return { ...state, quote };
    }
    case QUOTE_BUY_VOLUME_SET: {
      const { buy_volume } = action.payload
      const { quote } = {...state}
      quote.buy_volume = buy_volume;
      return { ...state, quote };
    }
    case QUOTE_SELL_VOLUME_SET: {
      const { sell_volume } = action.payload
      const { quote } = {...state}
      quote.sell_volume = sell_volume;
      return { ...state, quote };
    }
    case QUOTE_OPEN_TRADE_SET: {
      const { openTrade } = action.payload
      return { ...state, openTrade };
    }
    case TRADES_CREATE: {
      const { trade } = action.payload
      return { ...state, trade };
    }
    case TRADES_GET: {
      const { trades } = action.payload
      return { ...state, trades };
    }
    case CURRENCIES_GET: {
      const { currencies } = action.payload
      return { ...state, currencies };
    }
    case TRADE_DETAILS_GET: {
      const { trade } = action.payload
      return { ...state, trade };
    }
    
    default: {
      return state;
    }
  }
};

export default trades;
