import apiClient from '../../utils/apiClient'
import { API_CALL_STATUS } from '../../utils/apiCallsStatus';

import { 
  API_CALL_ACTION,
  CURRENT_USER_GET,
  USER_ACCOUNT_GET
} from './types';

export const receiveApiError = error => ({ type: API_CALL_ACTION, payload: { status: API_CALL_STATUS.ERROR, apiError: error.message } });
export const receiveCurrentUserResponse = user => ({ type: CURRENT_USER_GET, payload: { user } });
export const receiveUserAccountResponse = account => ({ 
  type: USER_ACCOUNT_GET, payload: { 
    available: account.available_balance, 
    balance: account.balance } 
});

export const getCurrentUser = () => {
  return async (dispatch) => {
    try {
      const user = await apiClient.getCurrentUser();
      dispatch(receiveCurrentUserResponse(user));
    } catch (error) {
      dispatch(receiveApiError(error));
    }
  }
};

export const getUserAccount = () => {
  return async (dispatch, getState) => {
    const { userId } = getState().account;
    if (!userId) return;
    try {
      const account = await apiClient.getUserAccount(userId);
      dispatch(receiveUserAccountResponse(account));
    } catch (error) {
      dispatch(receiveApiError(error));
    }
  }
};