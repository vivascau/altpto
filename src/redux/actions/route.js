import { 
  HEADER_TITLE_SET,
  HEADER_BACK_BUTTON_SET,
  FOOTER_VISIBLE_SET
} from './types';

export const headerTitleSet = title => ({ type: HEADER_TITLE_SET, payload: { title } });
export const headerWithBackButtonSet = withBackButton => ({ type: HEADER_BACK_BUTTON_SET, payload: { withBackButton } });
export const footerVisibleSet = visible => ({ type: FOOTER_VISIBLE_SET, payload: { visible } });

export const onPageWithFooterAndBackButton = () => {
  return (dispatch) => {
    dispatch(headerWithBackButtonSet(true))
    dispatch(showFooter());
  }
}
export const hideFooter = () => {
  return (dispatch) => {
    dispatch(footerVisibleSet(false));
  }
}
export const showFooter = () => {
  return (dispatch) => {
    dispatch(footerVisibleSet(true));
  }
}
export const withBackButton = () => {
  return (dispatch) => {
    dispatch(headerWithBackButtonSet(true));
  }
}
export const withoutBackButton = () => {
  return (dispatch) => {
    dispatch(headerWithBackButtonSet(false));
  }
}
