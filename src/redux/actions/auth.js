import apiClient from '../../utils/apiClient';
import { API_CALL_STATUS } from '../../utils/apiCallsStatus';

import { 
  SET_AUTH_EMAIL, 
  SET_AUTH_PASSWORD,
  SET_AUTH_MFA, 
  SET_AUTH_TOKEN, 
  AUTH_LOGIN_POST,
  AUTH_LOGIN,
  AUTH_LOGOUT
} from './types';

export const AUTH_LOGIN_STATE = API_CALL_STATUS

export const setEmail = email => ({ type: SET_AUTH_EMAIL, payload: { email } });
export const setPassword = password => ({ type: SET_AUTH_PASSWORD, payload: { password } });
export const setMFA = mfa => ({ type: SET_AUTH_MFA, payload: { mfa } });
export const setToken = token => ({ type: SET_AUTH_TOKEN, payload: { token } });

export const authtLoginPost = () => ({ type: AUTH_LOGIN_POST, payload: { status: AUTH_LOGIN_STATE.PROCESSING } })
export const receiveLoginError = error => ({ type: AUTH_LOGIN_POST, payload: { status: AUTH_LOGIN_STATE.ERROR, error: error.message } });
export const receiveLoginSuccess = () => ({ type: AUTH_LOGIN_POST, payload: { status: AUTH_LOGIN_STATE.SUCCESS, isLoggedIn: true } });
export const clearLoginError = () => ({ type: AUTH_LOGIN_POST, payload: { status: '', error: '' } });
export const receiveLoginResponse = token => ({ type: AUTH_LOGIN, payload: { token } });

export const postLogin = () => {
  return async (dispatch, getState) => {
    const { email, password, mfa } = getState().auth;
    dispatch(authtLoginPost());
    try {
      const token = await apiClient.loginUser(email, password, mfa);
      dispatch(receiveLoginResponse(token));
      dispatch(refreshAuthBearer())
      dispatch(receiveLoginSuccess());
    } catch (error) {
      dispatch(receiveLoginError(error));
    }
  }
};

export const logout = () => {
  return (dispatch) => {
    localStorage.setItem('TOKEN', null)
    dispatch((() => ({ type: AUTH_LOGOUT })));
  }
};

export const refreshAuthBearer = () => {
  return async (dispatch, getState) => {
    const { token } = getState().auth;
    apiClient.updateBearerToken(token);
  }
}
