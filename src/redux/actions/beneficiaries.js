import apiClient from '../../utils/apiClient'
import { API_CALL_STATUS } from '../../utils/apiCallsStatus';

import { 
  API_CALL_ACTION,
  BENEFICIARIES_GET_ALL,
  BENEFICIARIES_GET_ONE,
  BENEFICIARIES_CREATE
} from './types';

export const receiveApiError = error => ({ type: API_CALL_ACTION, payload: { status: API_CALL_STATUS.ERROR, apiError: error.message } });
export const receiveAllBeneficiariesResponse = beneficiaries => ({ type: BENEFICIARIES_GET_ALL, payload: { beneficiaries } });
export const receiveBeneficiaryResponse = beneficiary => ({ type: BENEFICIARIES_GET_ONE, payload: { beneficiary } });
export const createBeneficiaryResponse = beneficiary => ({ type: BENEFICIARIES_CREATE, payload: { beneficiary } });

export const getAllBeneficiaries = () => {
  return async (dispatch) => {
    try {
      const beneficiaries = await apiClient.getBeneficiaries();
      dispatch(receiveAllBeneficiariesResponse(beneficiaries));
    } catch (error) {
      dispatch(receiveApiError(error));
    }
  }
};

export const getBeneficiary = (id) => {
  return async (dispatch) => {
    try {
      const beneficiary = await apiClient.getBeneficiary(id);
      dispatch(receiveBeneficiaryResponse(beneficiary));
    } catch (error) {
      dispatch(receiveApiError(error));
    }
  }
};

export const createBeneficiary = (name, display_name, crypto_currency, crypto_address) => {
  return async (dispatch) => {
    try {
      const beneficiary = await apiClient.createBeneficiary(name, display_name, crypto_currency, crypto_address);
      dispatch(createBeneficiaryResponse(beneficiary));
    } catch (error) {
      dispatch(receiveApiError(error));
    }
  }
};
