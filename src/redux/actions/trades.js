import apiClient from '../../utils/apiClient'
import { API_CALL_STATUS } from '../../utils/apiCallsStatus';

import { 
  API_CALL_ACTION,
  QUOTES_CREATE,
  QUOTE_IS_VALID_SET,
  QUOTE_BENEFICIARY_SET,
  QUOTE_BUY_CURRENCY_SET,
  QUOTE_SELL_CURRENCY_SET,
  QUOTE_FIXED_SIDE_SET,
  QUOTE_BUY_VOLUME_SET,
  QUOTE_SELL_VOLUME_SET,
  QUOTE_OPEN_TRADE_SET,
  TRADES_CREATE,
  TRADES_GET,
  TRADE_DETAILS_GET,
  CURRENCIES_GET,
} from './types';

export const receiveApiError = error => ({ type: API_CALL_ACTION, payload: { status: API_CALL_STATUS.ERROR, apiError: error.message } });
export const receiveApiSuccess = () => ({ type: API_CALL_ACTION, payload: { status: API_CALL_STATUS.SUCCESS } });
export const apiProcessingAction = () => ({ type: API_CALL_ACTION, payload: { status: API_CALL_STATUS.PROCESSING } });
export const createQuotesResponse = quote => ({ type: QUOTES_CREATE, payload: { quote } });
export const createTradeResponse = trade => ({ type: TRADES_CREATE, payload: { trade } });
export const getAllTradesResponse = trades => ({ type: TRADES_GET, payload: { trades } });
export const setTradeResponse = trade => ({ type: TRADE_DETAILS_GET, payload: { trade } });

export const createQuote = (beneficiary_id, buy_currency, sell_currency, fixed_side, buy_volume, sell_volume) => {
  return async (dispatch) => {
    try {
      dispatch(apiProcessingAction())
      const quote = await apiClient.createQuote(beneficiary_id, buy_currency, sell_currency, fixed_side, buy_volume, sell_volume);
      dispatch(createQuotesResponse(quote));
      dispatch(receiveApiSuccess())
    } catch (error) {
      dispatch(receiveApiError(error));
    }
  }
};

export const createTrade = (quote_id, beneficiary_id, buy_currency, buy_volume, sell_currency, sell_volume, settlement_fee, price) => {
  return async (dispatch) => {
    try {
      dispatch(apiProcessingAction())
      const trade = await apiClient.createTrade(quote_id, beneficiary_id, buy_currency, buy_volume, sell_currency, sell_volume, settlement_fee, price);
      dispatch(createTradeResponse(trade));
      dispatch(receiveApiSuccess())
    } catch (error) {
      dispatch(receiveApiError(error));
    }
  }
};

export const getTrade = (tradeId) => {
  return async (dispatch) => {
    try {
      dispatch(apiProcessingAction())
      const trade = await apiClient.getTrade(tradeId);
      dispatch(setTradeResponse(trade));
      dispatch(receiveApiSuccess())
    } catch (error) {
      dispatch(receiveApiError(error));
      dispatch(setTradeResponse({}));
    }
  }
};

export const getTrades = () => {
  return async (dispatch) => {
    try {
      dispatch(apiProcessingAction())
      const trades = await apiClient.getTrades();
      dispatch(getAllTradesResponse(trades));
      dispatch(receiveApiSuccess())
    } catch (error) {
      dispatch(receiveApiError(error));
    }
  }
};
export const getCurrencies = () => {
  return async (dispatch) => {
    try {
      const currencies = await apiClient.getCurrencies();
      dispatch((currencies => ({ type: CURRENCIES_GET, payload: { currencies } }))(currencies));
    } catch (error) {
      dispatch(receiveApiError(error));
    }
  }
};
export const quoteBeneficiarySet = (beneficiary_id) => {
  return async (dispatch) => {
    dispatch((beneficiary_id => ({ type: QUOTE_BENEFICIARY_SET, payload: { beneficiary_id } }))(beneficiary_id));
  }
};
export const quoteIsValidSet = (quoteIsValid) => {
  return async (dispatch) => {
    dispatch((quoteIsValid => ({ type: QUOTE_IS_VALID_SET, payload: { quoteIsValid } }))(quoteIsValid));
  }
};
export const quoteBuyCurrencySet = (buy_currency) => {
  return async (dispatch) => {
    dispatch((buy_currency => ({ type: QUOTE_BUY_CURRENCY_SET, payload: { buy_currency } }))(buy_currency));
  }
};
export const quoteSellCurrencySet = (sell_currency) => {
  return async (dispatch) => {
    dispatch((sell_currency => ({ type: QUOTE_SELL_CURRENCY_SET, payload: { sell_currency } }))(sell_currency));
  }
};
export const quoteFixedSideSet = (fixed_side) => {
  return async (dispatch) => {
    dispatch((fixed_side => ({ type: QUOTE_FIXED_SIDE_SET, payload: { fixed_side } }))(fixed_side));
  }
};
export const quoteBuyVolumeSet = (buy_volume) => {
  return async (dispatch) => {
    dispatch((buy_volume => ({ type: QUOTE_BUY_VOLUME_SET, payload: { buy_volume } }))(buy_volume));
  }
};
export const quoteSellVolumeSet = (sell_volume) => {
  return async (dispatch) => {
    dispatch((sell_volume => ({ type: QUOTE_SELL_VOLUME_SET, payload: { sell_volume } }))(sell_volume));
  }
};
export const quoteOpenTradeSet = (openTrade) => {
  return async (dispatch) => {
    dispatch((openTrade => ({ type: QUOTE_OPEN_TRADE_SET, payload: { openTrade } }))(openTrade));
  }
};